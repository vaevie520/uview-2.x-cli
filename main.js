import App from './App'
import store from '@/store'

// #ifndef VUE3
import Vue from 'vue'

import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

// 修改$u.props对象的属性
uni.$u.setConfig({
  props: {
    image: {
      showMenuByLongpress: false,
    },
    button: {
      throttleTime: 1000,
    },
  },
})

import '@/utils/filters'

import themeMixin from '@/utils/themeMixin'
Vue.mixin(themeMixin)

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
  store,
  ...App,
})
require('@/utils/http.instance.js')(app)
require('@/utils/common.js')(app)
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app,
  }
}
// #endif
