/**
 * @description 请求封装，使其适用uview内的请求使用规则
 * @param {String} url 请求url
 * @param {String} method 请求方法
 * @param {Object} params 请求参数，会拼在url后面
 * @param {Object} data 请求体，会放在body中，json格式
 * @param {Object} header 自定义请求头
 * @param {String} responseType 响应类型，只在get、post中有效
 * @param {String} dataType 数据类型，只在get、post中有效
 * @param {Function} getTask download请求任务
 * @param {Object} custom 自定义参数 {loading:请求中是否弹出loading提示,version:接口版本号,noToast:返回错误时不弹出错误提示}
 * @returns {Function}
 */

const instance = ({ url, method, params, data, header, responseType, dataType, getTask, ...custom }) => {
  if (['get', 'GET'].includes(method)) {
    return uni.$u.http.get(url, { params, header, responseType, dataType, custom })
  } else if (['post', 'POST'].includes(method)) {
    return uni.$u.http.post(url, data, { params, header, responseType, dataType, custom })
  } else if (['download', 'DOWNLOAD'].includes(method)) {
    return uni.$u.http.download(url, { params, header, getTask, custom })
  }
}

export default instance
