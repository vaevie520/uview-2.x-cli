import { mapGetters } from 'vuex'
export default {
  computed: {
    ...mapGetters({
      mainColor: 'system/mainColor',
    }),
    customTheme() {
      return {
        '--main-color': this.mainColor,
      }
    },
  },
}
