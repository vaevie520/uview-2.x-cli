import request from '@/utils/request'

// 通过code获取openid等信息
export function getOpenid(params) {
  return request({
    url: '/wxuser/get_openid',
    method: 'get',
    params,
    version: '2.0.0',
  })
}

// 登录
export function login(params, data) {
  return request({
    url: '/wxuser/login',
    method: 'post',
    params,
    data,
    version: '2.0.0',
  })
}
