/**
 * @description 导入所有 vuex 模块，自动加入namespaced:true，用于解决vuex命名冲突，请勿修改。
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const modules = {}
const files = require.context('./modules', false, /\.js$/)
files.keys().forEach(key => {
  modules[key.replace(/(modules|\/|\.|js)/g, '')] = {
    ...files(key).default,
    namespaced: true,
  }
})

const store = new Vuex.Store({
  modules,
})

store.setState = (namespace, name, value) => store.commit(`${namespace}/setState`, { name, value })

// 使用方法 this.$store.setState(模块名，属性名，属性值) 。 可快速改变vuex中的状态，如果该属性在saveStateKeys有存储，则同时会改变缓存中该值

export default store
