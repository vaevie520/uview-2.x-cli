// 保存变量到本地存储中
export const saveLifeData = function(lifeDataNamespace, saveStateKeys, key, value) {
	// 判断变量名是否在需要存储的数组中
	if (saveStateKeys.includes(key)) {
		// 获取本地存储的lifeData对象，将变量添加到对象中
		let tmp = uni.getStorageSync(lifeDataNamespace)
		// 第一次打开APP，不存在lifeData变量，故放一个{}空对象
		tmp = tmp ? tmp : {}
		tmp[key] = value
		// 执行这一步后，所有需要存储的变量，都挂载在本地的lifeData对象中
		uni.setStorageSync(lifeDataNamespace, tmp)
	}
}

export const setState = function(state, payload) {
	// 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
	let nameArr = payload.name.split('.')
	let saveKey = ''
	let len = nameArr.length
	if (nameArr.length >= 2) {
		let obj = state[nameArr[0]]
		for (let i = 1; i < len - 1; i++) {
			obj = obj[nameArr[i]]
		}
		obj[nameArr[len - 1]] = payload.value
		saveKey = nameArr[0]
	} else {
		state[payload.name] = payload.value
		saveKey = payload.name
	}
	// 保存变量到本地，见顶部函数定义
	saveLifeData(state.lifeDataNamespace, state.saveStateKeys, saveKey, state[saveKey])
}
