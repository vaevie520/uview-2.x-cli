/**
 * @description 用户数据
 */
import { setState } from '@/store/lifeData'
const lifeDataNamespace = 'userLifeData'
const lifeData = uni.getStorageSync(lifeDataNamespace) || {}

const state = () => ({
  lifeDataNamespace,
  saveStateKeys: ['accessToken', 'tokenType', 'refreshToken', 'userInfo', 'isLogin'], // 需要永久存储，且下次APP启动需要取出的，在state中的变量名
  accessToken: lifeData.accessToken || '',
  refreshToken: lifeData.refreshToken || '',
  tokenType: lifeData.tokenType || '',
  userInfo: lifeData.userInfo || {},
  isLogin: lifeData.isLogin || false,
})

const getters = {
  accessToken: state => state.accessToken,
  refreshToken: state => state.refreshToken,
  tokenType: state => state.tokenType,
  userInfo: state => state.userInfo,
  isLogin: state => state.isLogin,
}

const mutations = {
  setState,
}

const actions = {}
export default {
  state,
  getters,
  mutations,
  actions,
}
