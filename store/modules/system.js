/**
 * @description 系统数据
 */
import { setState } from '@/store/lifeData'
const lifeDataNamespace = 'systemLifeData'
const lifeData = uni.getStorageSync(lifeDataNamespace) || {}

const state = () => ({
  lifeDataNamespace,
  saveStateKeys: ['mainColor'],
  mainColor: lifeData.mainColor || '#3c9cff',
})

const getters = {
  mainColor: state => state.mainColor,
}

const mutations = {
  setState,
}
const actions = {}
export default {
  state,
  getters,
  mutations,
  actions,
}
